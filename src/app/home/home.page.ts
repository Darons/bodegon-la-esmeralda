import { Component } from '@angular/core';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { Contacts } from '@ionic-native/contacts/ngx';
import { AppAvailability } from '@ionic-native/app-availability/ngx';
import { InAppBrowser, InAppBrowserObject } from '@ionic-native/in-app-browser/ngx';
import { Platform } from '@ionic/angular';

const message = 'Todo en licores nacionales e importados, delicateses y más, síguelos como @laesmeralda_ca Visite www.laesmeralda.com.ve';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

	constructor(
		private socialSharing: SocialSharing,
		private contacts: Contacts,
		private appAvailability: AppAvailability, 
		private platform: Platform, 
		private inAppBrowser: InAppBrowser
	) {}

	async shareTwitter() {
    this.socialSharing.shareViaTwitter(message).then(() => {
      // Success
    }).catch((e) => {
      // Error!
    });
  }
 
  async shareWhatsApp() {
    this.socialSharing.shareViaWhatsApp(message).then(() => {
      // Success
    }).catch((e) => {
      // Error!
    });
  }
 
  async shareSMS() {
		let numbers: string = '';
		this.contacts.pickContact().then(_ => {
			_.phoneNumbers.forEach(__ => numbers += __.value + ',')
		});
 
    this.socialSharing.shareViaSMS(message, numbers).then(() => {
    }).catch((e) => {
      // Error!
    });
  }
 
  async shareFacebook() {
    this.socialSharing.shareViaFacebook(message).then(() => {
    }).catch((e) => {
      // Error!
    });
  }

	async shareInstagram() {
    this.socialSharing.shareViaInstagram(message, null).then(() => {
    }).catch((e) => {
      // Error!
    });
	}
	
	// pass in the app name and the name of the user/page
	openUrl(app: string, name: string, fbUrl ? : string) {
		switch (app) {
			case 'facebook':
				this.launchApp('fb://', 'com.facebook.katana', 'fb://facewebmodal/f?href=' + fbUrl, 'https://www.facebook.com/' + name);
				break;
			case 'instagram':
				this.launchApp('instagram://', 'com.instagram.android', 'instagram://user?username=' + name, 'https://www.instagram.com/' + name);
				break;
			case 'twitter':
				this.launchApp('twitter://', 'com.twitter.android', 'twitter://user?screen_name=' + name, 'https://twitter.com/' + name);
				break;
			default:
				break;
		}
	}

	private launchApp(iosApp: string, androidApp: string, appUrl: string, webUrl: string) {
		let app: string;
		// check if the platform is ios or android, else open the web url
		if (this.platform.is('ios')) {
			app = iosApp;
		} else if (this.platform.is('android')) {
			app = androidApp;
		} else {
			const browser: InAppBrowserObject = this.inAppBrowser.create(webUrl, '_system');
			return;
		}
		this.appAvailability.check(app).then(
			() => {
				// success callback, the app exists and we can open it
				const browser: InAppBrowserObject = this.inAppBrowser.create(appUrl, '_system');
			},
			() => {
				// error callback, the app does not exist, open regular web url instead
				const browser: InAppBrowserObject = this.inAppBrowser.create(webUrl, '_system');
			}
		);
	}
}
